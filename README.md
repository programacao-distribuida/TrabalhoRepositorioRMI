# TrabalhoRepositorioRMI
Trabalho com o intuito de usar a biblioteca RMI para implementar programação distribuida.
## Problemática
Implementar um servidor de busca em repositórios que permita clientes armazenarem palavras em repositórios distribuídos (usar pelo menos 3 repositórios) e também solicitarem ao servidor que consulte qual(is) repositório(s) contém uma determinada palavra.

## Resolução
Existem 3 principais entidades: Cliente, Servidor e Repositório.
* O cliente manda informações para o servidor. Tais como, armazenar palavras, buscar quais repositórios armazenam a palavra buscada e quais repositórios existem.
* O servidor decide se o repositório guarda ou não a palavra e a manda para o repositório para ser guardada. O servidor também manda a palavra a ser buscada para o repositório.
* O repositório responde ao cliente se guarda a palavra.

## Como Executar
Existem dois modos de se executar: Via Intellij IDEA ou via linha de comando. Para ser executado via linha de comando, JARs foram criados. Esses jars podem ser encontrados dentro da pastas que se encontram em out/artifacts.

Existem argumentos que podem ser passados ao executar: 
* -ips ou --IpServidor [] => IP do Servidor
* -ps ou --PortaServidor [] => Porta do Servidor
* -ns ou --NomeServidor [] => Nome do Servidor
* -ipr ou --IpRepositorio [] => IP do Repositório
* -pr ou --PortaRepositorio [] => Porta do Repositório
* -nr ou --NomeRepositorio [] => Nome do Repositório

Obs.: Se tais argumentos não forem passados, então serão perguntados no console.

### Exemplos
* Servidor
<br>`java -jar ServidorRMI.jar -ips 192.168.1.14 -ps 4000 -ns Teste`
<br>`java -jar ServidorRMI.jar`
<br>(IP: 0.0.0.0, Porta: 3000, Nome: Servidor)
* Repositório
<br>`java -jar RepositorioRMI.jar -ips 192.168.1.14 -ps 4000 -ns Teste -ipr 192.168.1.20 -pr 4001 -nr AnneFrank`
<br>`java -jar RepositorioRMI.jar`
* Cliente
<br>`java -jar ClienteRMI.jar -ips 192.168.1.14 -ps 4000 -ns Teste`
<br>`java -jar ClienteRMI.jar`
## Autores
Aroldo Felix (junioraroldo37@gmail.com) <br>
Vitor Henrique (vitorhenrique908@gmail.com)