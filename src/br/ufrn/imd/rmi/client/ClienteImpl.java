package br.ufrn.imd.rmi.client;

import br.ufrn.imd.rmi.interfaces.InterfaceCliente;
import br.ufrn.imd.rmi.interfaces.InterfaceRepositorio;
import br.ufrn.imd.rmi.utils.StringUtils;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Implementação do cliente
 *
 * @author Aroldo Felix
 */
public class ClienteImpl extends UnicastRemoteObject implements InterfaceCliente {

    private static final long serialVersionUID = 3986832337945698321L;

    public ClienteImpl() throws RemoteException {
    }

    /**
     * Onde a palavra está guardada
     *
     * @param repositorio Repositório que guarda a palavra
     * @param palavra     Palavra sendo guardada
     * @throws RemoteException Se a conexão falhar
     */
    @Override
    public void print(InterfaceRepositorio repositorio, String palavra) throws RemoteException {
        StringUtils.print("\t==================\n" +
                repositorio.getNome() + " guarda a palavra: " + palavra +
                "\n\t==================");
    }

    /**
     * Listar os repositórios
     *
     * @param repositorio
     * @throws RemoteException
     */
    @Override
    public void print(InterfaceRepositorio repositorio) throws RemoteException {
        StringUtils.print("* " + repositorio.getNome());
    }
}
