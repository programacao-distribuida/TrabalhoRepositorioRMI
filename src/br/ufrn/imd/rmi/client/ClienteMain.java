package br.ufrn.imd.rmi.client;

import br.ufrn.imd.rmi.exceptions.EnderecoInvalidException;
import br.ufrn.imd.rmi.exceptions.ServidorNaoEncontradoException;
import br.ufrn.imd.rmi.exceptions.URLMalFormadaException;
import br.ufrn.imd.rmi.interfaces.InterfaceCliente;
import br.ufrn.imd.rmi.interfaces.InterfaceServidor;
import br.ufrn.imd.rmi.utils.StringErrorUtils;
import br.ufrn.imd.rmi.utils.StringUtils;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

import static br.ufrn.imd.rmi.utils.ServidorUtils.connect;
import static br.ufrn.imd.rmi.utils.ServidorUtils.getEnderecoServidor;
import static br.ufrn.imd.rmi.utils.StringUtils.print;

/**
 * Main para rodar o cliente
 *
 * @author Aroldo Felix
 */
public class ClienteMain {
    /**
     * Fun��o principal para se conectar ao servidor
     *
     * @param args -
     * @throws RemoteException                - Se a conex�o falhar
     * @throws EnderecoInvalidException       - Endere�o inv�lido do servidor
     * @throws URLMalFormadaException         - URL mal informada
     * @throws ServidorNaoEncontradoException - Servidor n�o encontrado
     */
    @SuppressWarnings("resource")
    public static void main(String[] args) throws RemoteException, EnderecoInvalidException, URLMalFormadaException, ServidorNaoEncontradoException {
        // Recupera endere�o do servidor
        String enderecoServidor = getEnderecoServidor(Arrays.asList(args));
        // Recupera o servidor
        InterfaceServidor servidor = connect(enderecoServidor);
        // Armazena ou busca palavras
        run(servidor);
    }

    /**
     * Realiza as opera��es de Armazenar e Buscar palavras
     *
     * @param servidor Servidor
     * @throws RemoteException Se a conex�o falhar
     */
    public static void run(InterfaceServidor servidor) throws RemoteException {
        Scanner scanner = new Scanner(System.in);
        InterfaceCliente cliente = new ClienteImpl();

        int opcao = 0;
        String palavra;

        while (opcao != -1) {

            print(StringUtils.OPCOES);
            try {
                opcao = scanner.nextInt();
                scanner.nextLine();

                switch (opcao) {
                    case 1:
                        print(StringUtils.GUARDAR_PALAVRA);
                        if (scanner.hasNextLine()) {
                            palavra = scanner.nextLine();
                            armazenar(servidor, palavra);
                        }
                        break;
                    case 2:
                        print(StringUtils.BUSCAR_PALAVRA);
                        if (scanner.hasNextLine()) {
                            palavra = scanner.nextLine();
                            buscar(servidor, cliente, palavra);
                        }
                        break;
                    case 3:
                        print("Reposit�rios: ");
                        servidor.getRepositorios(cliente);
                    case -1:
                        break;
                    default:
                        print(StringErrorUtils.OPCAO_INVALIDA);
                }
            } catch (InputMismatchException ignored) {
            }
        }
    }

    private static void armazenar(InterfaceServidor servidor, String frase) throws RemoteException {
        for (String palavra : frase.split(" ")) {
            servidor.armazenar(palavra);
        }
    }

    private static void buscar(InterfaceServidor servidor, InterfaceCliente cliente, String frase) throws RemoteException {
        for (String palavra : frase.split(" ")) {
            servidor.buscar(cliente, palavra);
        }
    }
}
