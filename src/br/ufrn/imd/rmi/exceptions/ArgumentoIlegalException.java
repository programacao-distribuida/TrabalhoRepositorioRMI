package br.ufrn.imd.rmi.exceptions;

public class ArgumentoIlegalException extends Exception {
    public ArgumentoIlegalException() {
    }

    public ArgumentoIlegalException(String message) {
        super(message);
    }
}
