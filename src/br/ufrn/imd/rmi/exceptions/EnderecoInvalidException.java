package br.ufrn.imd.rmi.exceptions;

import br.ufrn.imd.rmi.utils.StringErrorUtils;

public class EnderecoInvalidException extends Exception {
    public EnderecoInvalidException() {
        super(StringErrorUtils.ENDERECO_INVALIDO);
    }
}
