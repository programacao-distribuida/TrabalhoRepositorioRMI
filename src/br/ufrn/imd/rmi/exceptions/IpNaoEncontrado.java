package br.ufrn.imd.rmi.exceptions;

import br.ufrn.imd.rmi.utils.StringErrorUtils;

public class IpNaoEncontrado extends Exception {
    public IpNaoEncontrado() {
        super(StringErrorUtils.IP_NAO_ENCONTRADO);
    }
}
