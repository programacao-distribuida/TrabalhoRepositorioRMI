package br.ufrn.imd.rmi.exceptions;

import br.ufrn.imd.rmi.utils.StringErrorUtils;

public class NomeInvalidoException extends Exception {
    public NomeInvalidoException() {
        super(StringErrorUtils.NOME_INVALIDO);
    }
}
