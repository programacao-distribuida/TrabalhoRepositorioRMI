package br.ufrn.imd.rmi.exceptions;

import br.ufrn.imd.rmi.utils.StringErrorUtils;

public class ServidorNaoEncontradoException extends Exception {
    public ServidorNaoEncontradoException() {
        super(StringErrorUtils.SERVIDOR_NAO_ENCONTRADO);
    }
}
