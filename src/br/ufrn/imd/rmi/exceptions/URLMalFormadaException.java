package br.ufrn.imd.rmi.exceptions;

import br.ufrn.imd.rmi.utils.StringErrorUtils;

public class URLMalFormadaException extends Exception {
    public URLMalFormadaException() {
        super(StringErrorUtils.URL_MAL_FORMADA);
    }
}
