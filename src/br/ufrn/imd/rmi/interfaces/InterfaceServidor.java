package br.ufrn.imd.rmi.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface InterfaceServidor extends Remote {
    void armazenar(String palavra) throws RemoteException;

    void buscar(InterfaceCliente cliente, String palavra) throws RemoteException;

    void getRepositorios(InterfaceCliente cliente) throws RemoteException;

    void registrarRepositorio(InterfaceRepositorio repositorio) throws RemoteException;
}
