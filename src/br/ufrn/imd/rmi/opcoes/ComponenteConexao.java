package br.ufrn.imd.rmi.opcoes;

/**
 * Componente pra guardar ip, porta e nome
 */
public class ComponenteConexao {
    private String ip;
    private int porta;
    private String nome;

    public ComponenteConexao(String ip, int porta, String nome) {
        this.ip = ip;
        this.porta = porta;
        this.nome = nome;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPorta() {
        return porta;
    }

    public void setPorta(int porta) {
        this.porta = porta;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return ip + ":" + porta + "/" + nome;
    }
}
