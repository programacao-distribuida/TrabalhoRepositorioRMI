package br.ufrn.imd.rmi.opcoes;

public class Opcoes {
    private ComponenteConexao servidor;
    private ComponenteConexao repositorio;

    public Opcoes(String ipServidor, int portaServidor, String nomeServidor) {
        this.servidor = new ComponenteConexao(ipServidor, portaServidor, nomeServidor);
    }

    public Opcoes(String ipServidor, int portaServidor, String nomeServidor, String ipMaquina, int portaMaquina, String nomeMaquina) {
        this(ipServidor, portaServidor, nomeServidor);
        this.repositorio = new ComponenteConexao(ipMaquina, portaMaquina, nomeMaquina);
    }

    public ComponenteConexao getServidor() {
        return servidor;
    }

    public void setServidor(ComponenteConexao servidor) {
        this.servidor = servidor;
    }

    public ComponenteConexao getRepositorio() {
        return repositorio;
    }

    public void setRepositorio(ComponenteConexao repositorio) {
        this.repositorio = repositorio;
    }
}
