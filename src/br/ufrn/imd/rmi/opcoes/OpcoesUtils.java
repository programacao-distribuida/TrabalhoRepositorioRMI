package br.ufrn.imd.rmi.opcoes;

import br.ufrn.imd.rmi.exceptions.ArgumentoIlegalException;
import br.ufrn.imd.rmi.utils.OpcoesStringUtils;

import java.util.List;

/**
 * Classe usada para recuperar informações passadas por argumentos
 *
 * @author Aroldo Felix
 */
public class OpcoesUtils {

    public static Opcoes recuperarOpcoesServidor(List<String> args) throws ArgumentoIlegalException {
        String ipServidor = recuperarIpServidor(args);
        int portaServidor = recuperarPortaServidor(args);
        String nomeServidor = recuperarNomeServidor(args);

        return new Opcoes(ipServidor, portaServidor, nomeServidor);
    }

    public static Opcoes recuperarOpcoesCompletas(List<String> args) throws ArgumentoIlegalException {
        Opcoes opcoes = recuperarOpcoesServidor(args);

        // Informações do repositorio
        String ip = recuperarIpRepositorio(args);
        int porta = recuperarPortaRepositorio(args);
        String nome = recuperarNomeRepositorio(args);

        ComponenteConexao repositorio = new ComponenteConexao(ip, porta, nome);
        opcoes.setRepositorio(repositorio);

        return opcoes;
    }

    private static int recuperarCampo(List<String> args, String argumento1, String argumento2) throws ArgumentoIlegalException {
        String campo;
        if (args.contains(argumento1)) {
            campo = argumento1;
        } else if (args.contains(argumento2)) {
            campo = argumento2;
        } else {
            throw new ArgumentoIlegalException();
        }

        return args.indexOf(campo) + 1;
    }

    private static String recuperarIpServidor(List<String> args) throws ArgumentoIlegalException {
        try {
            int index = recuperarCampo(args, "-ips", "--IpServidor");
            return args.get(index);
        } catch (ArgumentoIlegalException ignored) {
            throw new ArgumentoIlegalException(OpcoesStringUtils.IP_SERVIDOR_NAO_INFORMADO);
        }
    }

    private static int recuperarPortaServidor(List<String> args) throws ArgumentoIlegalException {
        try {
            int index = recuperarCampo(args, "-ps", "--PortaServidor");
            return Integer.parseInt(args.get(index));
        } catch (ArgumentoIlegalException ignored) {
            throw new ArgumentoIlegalException(OpcoesStringUtils.PORTA_SERVIDOR_NAO_INFORMADA);
        }
    }

    private static String recuperarNomeServidor(List<String> args) throws ArgumentoIlegalException {
        try {
            int index = recuperarCampo(args, "-ns", "--NomeServidor");
            return args.get(index);
        } catch (ArgumentoIlegalException e) {
            throw new ArgumentoIlegalException(OpcoesStringUtils.NOME_SERVIDOR_NAO_INFORMADO);
        }
    }

    private static String recuperarIpRepositorio(List<String> args) throws ArgumentoIlegalException {
        try {
            int index = recuperarCampo(args, "-ipr", "--IpRepositorio");
            return args.get(index);
        } catch (ArgumentoIlegalException ignored) {
            throw new ArgumentoIlegalException(OpcoesStringUtils.IP_REPOSITORIO_NAO_INFORMADO);
        }
    }

    private static int recuperarPortaRepositorio(List<String> args) throws ArgumentoIlegalException {
        try {
            int index = recuperarCampo(args, "-pr", "--PortaRepositorio");
            return Integer.parseInt(args.get(index));
        } catch (ArgumentoIlegalException ignored) {
            throw new ArgumentoIlegalException(OpcoesStringUtils.PORTA_REPOSITORIO_NAO_INFORMADA);
        }
    }

    private static String recuperarNomeRepositorio(List<String> args) throws ArgumentoIlegalException {
        try {
            int index = recuperarCampo(args, "-nr", "--NomeRepositorio");
            return args.get(index);
        } catch (ArgumentoIlegalException e) {
            throw new ArgumentoIlegalException(OpcoesStringUtils.NOME_REPOSITORIO_NAO_INFORMADO);
        }
    }
}
