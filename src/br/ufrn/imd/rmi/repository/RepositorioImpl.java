package br.ufrn.imd.rmi.repository;

import br.ufrn.imd.rmi.interfaces.InterfaceCliente;
import br.ufrn.imd.rmi.interfaces.InterfaceRepositorio;
import br.ufrn.imd.rmi.opcoes.ComponenteConexao;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashSet;
import java.util.Set;

/**
 * Classe para implementação do repositório
 *
 * @author Aroldo Felix
 */
public class RepositorioImpl extends UnicastRemoteObject implements InterfaceRepositorio {
    private static final long serialVersionUID = -1553069973323620029L;

    private String nome;
    private String ip;
    private Integer porta;

    private final Set<String> palavras;

    public RepositorioImpl() throws RemoteException {
        super();
        this.palavras = new HashSet<>();
    }

    public RepositorioImpl(String nome, String ip, Integer porta) throws RemoteException {
        this();
        this.nome = nome;
        this.ip = ip;
        this.porta = porta;
    }

    public RepositorioImpl(ComponenteConexao repositorio) throws RemoteException {
        this(repositorio.getNome(), repositorio.getIp(), repositorio.getPorta());
    }

    /**
     * Armazena uma palavra no repositório
     *
     * @param palavra - Palavra a ser salva
     * @throws RemoteException - Se a conexão falhar
     */
    @Override
    public void armazenar(String palavra) throws RemoteException {
        this.palavras.add(palavra);
    }

    /**
     * Busca a palavra no repositório
     *
     * @param cliente - Cliente que está buscando
     * @param palavra - Palavra sendo buscada
     * @throws RemoteException - Se a conexão falhar
     */
    @Override
    public void buscar(InterfaceCliente cliente, String palavra) throws RemoteException {
        if (this.palavras.contains(palavra)) {
            cliente.print(this, palavra);
        }
    }

    /**
     * Nome do repositório
     *
     * @return Nome do repositório
     * @throws RemoteException - Se a conexão falhar
     */
    @Override
    public String getNome() throws RemoteException {
        return this.nome;
    }

    /**
     * IP
     *
     * @return IP
     * @throws RemoteException - Se a conexão falhar
     */
    @Override
    public String getIp() throws RemoteException {
        return this.ip;
    }

    /**
     * Endereço formatado
     *
     * @return Endereço formatado
     * @throws RemoteException - Se a conexão falhar
     */
    @Override
    public String getEndereco() throws RemoteException {
        return "rmi://" + this.ip + ":" + this.porta + "/" + this.nome;
    }

    /**
     * Porta do endereço
     *
     * @return Porta
     * @throws RemoteException - Se a conexão falhar
     */
    @Override
    public Integer getPorta() throws RemoteException {
        return this.porta;
    }
}
