package br.ufrn.imd.rmi.repository;

import br.ufrn.imd.rmi.exceptions.*;
import br.ufrn.imd.rmi.interfaces.InterfaceRepositorio;
import br.ufrn.imd.rmi.interfaces.InterfaceServidor;
import br.ufrn.imd.rmi.opcoes.Opcoes;
import br.ufrn.imd.rmi.opcoes.OpcoesUtils;
import br.ufrn.imd.rmi.utils.ServidorUtils;
import br.ufrn.imd.rmi.utils.StringErrorUtils;
import br.ufrn.imd.rmi.utils.StringUtils;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import static br.ufrn.imd.rmi.utils.ServidorUtils.connect;
import static br.ufrn.imd.rmi.utils.StringUtils.print;

/**
 * Main para rodar o reposit�rio
 *
 * @author Aroldo Felix
 */
public class RepositorioMain {

    /**
     * Fun��o principal para criar reposit�rio e registrar no servidor
     *
     * @param args -
     * @throws RemoteException                - Se a conex�o falhar
     * @throws EnderecoInvalidException       - Endere�o do servidor incorreto
     * @throws URLMalFormadaException         - URL mal informada
     * @throws ServidorNaoEncontradoException - Se o servidor n�o for encontrado
     * @throws NomeInvalidoException          - Nome do reposit�rio inv�lido
     */
    @SuppressWarnings({"resource", "null"})
    public static void main(String[] args) throws RemoteException, EnderecoInvalidException, URLMalFormadaException, ServidorNaoEncontradoException, NomeInvalidoException {
        System.setProperty("java.rmi.server.hostname", "0.0.0.0");

        // Recupera endere�o do servidor e faz a conex�o
        String enderecoServidor = ServidorUtils.getEnderecoServidor(Arrays.asList(args));
        InterfaceServidor servidor = connect(enderecoServidor);

        // Cria e registra o reposit�rio
        InterfaceRepositorio repositorio = createRepository(Arrays.asList(args));
        registerRepository(repositorio, servidor);
        System.out.println("Repositorio " + repositorio.getNome() + " rodando na porta " + repositorio.getPorta() + ".");
    }

    /**
     * Cria o reposit�rio
     *
     * @return Reposit�rio
     * @throws NomeInvalidoException - Nome vazio ou em branco
     * @throws RemoteException       - N�o construiu o reposit�rio
     */
    public static InterfaceRepositorio createRepository(List<String> args) throws NomeInvalidoException, RemoteException {
        try {
            Opcoes opcoes = OpcoesUtils.recuperarOpcoesCompletas(args);
            return new RepositorioImpl(opcoes.getRepositorio());
        } catch (ArgumentoIlegalException ignored) {
            // Requisita o usu�rio o nome do reposit�rio
            Scanner scanner = new Scanner(System.in);
            print(StringUtils.NOME_REPOSITORIO);
            String nomeRepositorio = scanner.nextLine();

            if (nomeRepositorio.isBlank() || nomeRepositorio.isEmpty()) {
                throw new NomeInvalidoException();
            }

            while (nomeRepositorio.contains(" ")) {
                print(StringErrorUtils.NOME_INVALIDO);
                print(StringErrorUtils.CONTEM_ESPACO);
                nomeRepositorio = scanner.nextLine();
            }

            // Requisita o usu�rio a porta do reposit�rio
            print(StringUtils.PORTA_REPOSITORIO);
            Integer portaRepositorio;
            try {
                portaRepositorio = scanner.nextInt();
            } catch (InputMismatchException ignored1) {
                portaRepositorio = scanner.nextInt();
            }

            return new RepositorioImpl(nomeRepositorio, "0.0.0.0", portaRepositorio);
        }
    }

    /**
     * Faz o registro do reposit�rio no servidor
     *
     * @param repositorio Reposit�rio
     * @param servidor    Servidor
     * @throws RemoteException        - Se o registro n�o pode ser exportado
     * @throws URLMalFormadaException - Se a URL n�o for apropriada
     */
    public static void registerRepository(InterfaceRepositorio repositorio, InterfaceServidor servidor) throws RemoteException, URLMalFormadaException {
        LocateRegistry.createRegistry(repositorio.getPorta());
        try {
            Naming.rebind(repositorio.getEndereco(), repositorio);
        } catch (MalformedURLException e) {
            throw new URLMalFormadaException();
        }

        // Adiciona o reposit�rio a lista dos reposit�rios que o servidor escuta.
        servidor.registrarRepositorio(repositorio);
    }
}
