package br.ufrn.imd.rmi.server;

import br.ufrn.imd.rmi.interfaces.InterfaceCliente;
import br.ufrn.imd.rmi.interfaces.InterfaceRepositorio;
import br.ufrn.imd.rmi.interfaces.InterfaceServidor;
import br.ufrn.imd.rmi.utils.StringErrorUtils;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static br.ufrn.imd.rmi.utils.StringUtils.print;

/**
 * Implementa��o do Servidor que gerencia os reposit�rios
 *
 * @author Aroldo Felix
 */
public class ServidorImpl extends UnicastRemoteObject implements InterfaceServidor {

    private static final long serialVersionUID = 3680032288912171665L;

    private volatile List<InterfaceRepositorio> repositorios = new ArrayList<>();

    public Random gerador;

    public ServidorImpl() throws RemoteException {
        this.gerador = new Random();
    }

    /**
     * Adiciona a palavra aleatoriamente e unicamente entre os reposit�rios dispon�veis.
     *
     * @param palavra - Palavra a ser guardada
     * @throws RemoteException - Se a conex�o falhar
     */
    @Override
    public void armazenar(String palavra) throws RemoteException {
        if (this.repositorios.size() == 0) {
            print(StringErrorUtils.NENHUM_REPOSITORIO);
        } else {
            boolean palavraGuardada = false;
            while (!palavraGuardada) {
                for (InterfaceRepositorio repositorio : repositorios) {
                    if (this.gerador.nextBoolean()) {
                        palavraGuardada = true;
                        repositorio.armazenar(palavra);
                    }
                }
            }
        }
    }

    /**
     * Busca entre o reposit�rios dispon�veis a palavra.
     *
     * @param cliente - Cliente que est� buscando a palavra
     * @param palavra - Palavra a ser buscada
     * @throws RemoteException - Se a conex�o falhar
     */
    @Override
    public void buscar(InterfaceCliente cliente, String palavra) throws RemoteException {
        if (this.repositorios.size() == 0) {
            print(StringErrorUtils.NENHUM_REPOSITORIO);
        } else {
            for (InterfaceRepositorio repositorio : repositorios) {
                repositorio.buscar(cliente, palavra);
            }
        }
    }

    /**
     * Mostra os reposit�rios
     *
     * @throws RemoteException - Se a conex�o falhar
     */
    @Override
    public void getRepositorios(InterfaceCliente cliente) throws RemoteException {
        for (InterfaceRepositorio repositorio : repositorios) {
            cliente.print(repositorio);
        }
    }


    /**
     * Adiciona um reposit�rio a lista.
     *
     * @param repositorio - Reposit�rio para ser adicionado
     * @throws RemoteException - Se a conex�o falhar
     */
    @Override
    public void registrarRepositorio(InterfaceRepositorio repositorio) throws RemoteException {
        this.repositorios.add(repositorio);
    }
}
