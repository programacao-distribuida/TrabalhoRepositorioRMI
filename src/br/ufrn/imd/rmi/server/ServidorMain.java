package br.ufrn.imd.rmi.server;

import br.ufrn.imd.rmi.exceptions.ArgumentoIlegalException;
import br.ufrn.imd.rmi.interfaces.InterfaceServidor;
import br.ufrn.imd.rmi.opcoes.ComponenteConexao;
import br.ufrn.imd.rmi.opcoes.OpcoesUtils;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.Arrays;

import static br.ufrn.imd.rmi.utils.StringUtils.print;

/**
 * Main para rodar o servidor
 *
 * @author Aroldo Felix
 */
public class ServidorMain {
    private static final String ip = "0.0.0.0";
    private static final int porta = 3000;
    private static final String nome = "Servidor";

    /**
     * Função principal para criar o servidor
     *
     * @param args -
     * @throws RemoteException       - Se a conexão falhar
     * @throws MalformedURLException - Se a url estiver inválida
     */
    public static void main(String[] args) throws RemoteException, MalformedURLException {
        try {
            ComponenteConexao componente = OpcoesUtils.recuperarOpcoesServidor(Arrays.asList(args)).getServidor();
            InterfaceServidor servidor = new ServidorImpl();
            System.setProperty("java.rmi.server.hostname", componente.getIp());
            LocateRegistry.createRegistry(componente.getPorta());
            Naming.rebind(construirRmiUrl(componente), servidor);
            print("Servidor Rodando no endereço: " + construirRmiUrl(componente) + ".");
        } catch (ArgumentoIlegalException e) {
            // Incializa o servidor
            InterfaceServidor servidor = new ServidorImpl();
            System.setProperty("java.rmi.server.hostname", ip);
            LocateRegistry.createRegistry(porta);
            Naming.rebind(construirRmiUrl(), servidor);
            print("Servidor Rodando no endereço: " + construirRmiUrl() + ".");
        }

    }

    /**
     * Constrói a URL no formato adequado
     *
     * @return URL no formato adequado
     */
    private static String construirRmiUrl() {
        return "rmi://" + ip + ":" + porta + "/" + nome;
    }

    /**
     * Constroi a URL no formato adequado
     *
     * @param servidor Servidor contendo IP, Porta e Nome
     * @return URL no formato adequado
     */
    private static String construirRmiUrl(ComponenteConexao servidor) {
        return "rmi://" + servidor.toString();
    }
}
