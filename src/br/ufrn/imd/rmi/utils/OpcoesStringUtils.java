package br.ufrn.imd.rmi.utils;

public class OpcoesStringUtils {
    public static String IP_SERVIDOR_NAO_INFORMADO = "\nIP do servidor não informado";
    public static String PORTA_SERVIDOR_NAO_INFORMADA = "\nPorta do servidor não informada";
    public static String NOME_SERVIDOR_NAO_INFORMADO = "\nNome do servidor não informado";

    public static String IP_REPOSITORIO_NAO_INFORMADO = "\nIP do repositório não informado";
    public static String PORTA_REPOSITORIO_NAO_INFORMADA = "\nPorta do repositório não informada";
    public static String NOME_REPOSITORIO_NAO_INFORMADO = "\nNome do repositório não informado";
}
