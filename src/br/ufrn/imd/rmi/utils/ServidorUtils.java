package br.ufrn.imd.rmi.utils;

import br.ufrn.imd.rmi.exceptions.ArgumentoIlegalException;
import br.ufrn.imd.rmi.exceptions.EnderecoInvalidException;
import br.ufrn.imd.rmi.exceptions.ServidorNaoEncontradoException;
import br.ufrn.imd.rmi.exceptions.URLMalFormadaException;
import br.ufrn.imd.rmi.interfaces.InterfaceServidor;
import br.ufrn.imd.rmi.opcoes.Opcoes;
import br.ufrn.imd.rmi.opcoes.OpcoesUtils;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Scanner;

import static br.ufrn.imd.rmi.utils.StringUtils.print;

public class ServidorUtils {
    /**
     * Captura o endereço do servidor no formato [ip]:[porta]/[nome]
     *
     * @return Endereço do servidor
     * @throws EnderecoInvalidException Endereço Inválido
     */
    public static String getEnderecoServidor(List<String> args) throws EnderecoInvalidException {
        try {
            Opcoes opcoes = OpcoesUtils.recuperarOpcoesServidor(args);
            return opcoes.getServidor().toString();
        } catch (ArgumentoIlegalException ignored) {
            Scanner scanner = new Scanner(System.in);
            print(StringUtils.ENDERECO_SERVIDOR);
            String endereco = scanner.nextLine();

            if (endereco.isBlank() || endereco.isEmpty()) {
                throw new EnderecoInvalidException();
            }

            while (endereco.contains(" ")) {
                print(StringErrorUtils.ENDERECO_INVALIDO);
                print(StringErrorUtils.CONTEM_ESPACO);
                endereco = scanner.nextLine();
            }

            return endereco;
        }
    }

    /**
     * Faz a conexão com o servidor de palavras
     *
     * @param enderecoServidor Endereço do servidor
     * @return Servidor
     * @throws RemoteException
     * @throws ServidorNaoEncontradoException Servidor não Encontrado
     * @throws URLMalFormadaException         URL mal formada
     */
    public static InterfaceServidor connect(String enderecoServidor) throws ServidorNaoEncontradoException, URLMalFormadaException, RemoteException {
        // Conecta-se com o servidor
        InterfaceServidor servidor;

        try {
            servidor = (InterfaceServidor) Naming.lookup("rmi://" + enderecoServidor);
        } catch (NotBoundException e) {
            throw new ServidorNaoEncontradoException();
        } catch (MalformedURLException e) {
            throw new URLMalFormadaException();
        }

        if (servidor == null) {
            throw new ServidorNaoEncontradoException();
        }

        return servidor;
    }
}
