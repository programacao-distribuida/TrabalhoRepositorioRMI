package br.ufrn.imd.rmi.utils;

public class StringErrorUtils {
    public static final String ENDERECO_INVALIDO = "ERRO: Endereço inválido!";
    public static final String SERVIDOR_NAO_ENCONTRADO = "ERRO: Nenhum servidor encontrado com esse endereço!";
    public static final String URL_MAL_FORMADA = "ERRO: URL mal formada!";
    public static final String IP_NAO_ENCONTRADO = "ERRO: IP não encontrado!";
    public static final String NENHUM_REPOSITORIO = "ERRO: Não existem repositórios nesse servidor!";
    public static final String NOME_INVALIDO = "ERRO: Nome Inválido!";
    public static final String CONTEM_ESPACO = "Possui espaço. Por favor, digite novamente: ";

    public static final String OPCAO_INVALIDA = "\nOpção inválida!";
}
