package br.ufrn.imd.rmi.utils;

public class StringUtils {
    public static final String OPCOES = "\nSelecione uma opção:\n\t1) Guardar palavra(s)\n\t2) Buscar palavra(s)\n\t3) Mostrar repositórios\nObs.: Para sair digite -1";
    public static final String GUARDAR_PALAVRA = "\nDigite a palavra que deseja guardar: ";
    public static final String BUSCAR_PALAVRA = "\nDigite a palavra que deseja buscar: ";
    public static final String NOME_REPOSITORIO = "\nDigite o nome do repositório: ";
    public static final String PORTA_REPOSITORIO = "\nDigite a porta do repositório: ";
    public static final String ENDERECO_SERVIDOR = "\nDigite o endereco do servidor no formato [ip]:[porta]/[nome]: ";
    public static final String ENDERECO_IP = "\nDigite o ip: ";

    public static void print(String mensagem) {
        System.out.println(mensagem);
    }
}
